syntax on
set nocp
set modeline
filetype plugin on

inoremap <expr> {     <SID>is_printable() ? "{" : "{}<Left>"
inoremap {<CR> {<CR>}<Esc>O
inoremap <expr> ( <SID>is_printable_not_brace() ? "(" : "()<Left>"
inoremap <expr> [ <SID>is_printable_not_brace() ? "[" : "[]<Left>"
inoremap <expr> " <SID>is_printable_not_brace() ? '"' : '""<Left>'
inoremap <expr> ' <SID>is_printable_not_brace() ? "'" : "''<Left>"

nnoremap <silent> <F7> :NERDTreeToggle<CR>
nnoremap <silent> <F8> :TagbarToggle<CR>
let g:tagbar_autofocus = 1

function! s:is_printable()
  let col = col('.') - 1
  return getline('.')[col] =~ '\p'
endfunction

function! s:is_printable_not_brace()
  let col = col('.') - 1
  if getline('.')[col] == ')'
    return 0
  else
    return getline('.')[col] =~ '\p'
endfunction

set nobackup
set nowritebackup
set noswapfile
set smartindent
set tabstop=4
set shiftwidth=4
set expandtab

colorscheme wombat256mod

highlight Pmenu ctermbg=white ctermfg=black
highlight PmenuSel ctermfg=white ctermbg=black

au CursorMovedI,InsertLeave * if pumvisible() == 0|silent! pclose|endif
set completeopt=menuone,menu,longest,preview

nnoremap <F3> :set hlsearch!<CR>
nnoremap <F2> :set list!<CR>
nnoremap <F6> :tabn<CR>
nnoremap <F5> :tabp<CR>

set laststatus=2
set statusline=[%n]\ %<%f\ %y%m%r%{\"[\".(&fenc==\"\"?&enc:&fenc).((exists(\"+bomb\")\ &&\&bomb)?\",B\":\"\").\"]\ \"}%=%l,%c%V\ %P
